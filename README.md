#mysqlFlashBack

[前提]<br>
1 mysql开启基于ROW模式二进制日志

[功能]<br>
1 读取mysql二进制日志并格式化到 tmpFILE/xxx.sql<br>
2 转换日志为可读可执行的sql文件<br>
3 可以把转换后的日志存放在mysql数据库<br>
  存放格式参见tab_binlog.sql<br>
  日志源和转换后的日志存放位置可在配置文件db.conf指定<br>
4 可以根据sql整理出它的逆sql(主要功能)<br>

[操作步骤]<br>
1 配置好db.conf<br>
2 windows下执行main.bat,linux下执行php binlog_output.php<br>



