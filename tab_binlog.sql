CREATE TABLE `tab_binlog` (
  `host` varchar(30) not null,
  `port` varchar(6) not null,
  `log_name` varchar(60) NOT null ,
  `pos` bigint(20)  NOT null ,
  `event_type` varchar(20) NOT null ,
  `server_id` bigint(20) NOT null ,
  `end_log_pos` bigint(20)  NOT null ,
  `info` LONGTEXT NOT null ,
  `start_time` varchar(45)  NOT null ,
  `db_name` varchar(45)  NOT null ,
  `table_name` varchar(45)  NOT null ,
  `sql_type` varchar(10)  NOT null ,
  `sql_text` LONGTEXT NOT null ,
  `reverse_sql` LONGTEXT NOT null 
) ENGINE=CSV DEFAULT CHARSET=utf8 COMMENT='binlog日志临时存储表'